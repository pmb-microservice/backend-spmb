<?php

namespace Database\Seeders;

use \App\Models\Prodi;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {   
        Prodi::create(
            ['nama' => 'Teknik Perminyakan'],
        );
        Prodi::create(
            ['nama' => 'Teknik Mesin'],
        );
        Prodi::create(
            ['nama' => 'Hubungan Internasional'],
        );
        Prodi::create(
            ['nama' => 'Psikologi'],
        );

        
    }
}
