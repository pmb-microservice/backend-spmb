<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('')->get('/prodi', function (Request $request) {
    return $request->user();
});


Route::resource('prodi', App\Http\Controllers\ProdiController::class);

Route::post('pendaftar/create', [App\Http\Controllers\PendaftarController::class, 'store']);
Route::resource('pendaftar', App\Http\Controllers\PendaftarController::class);
