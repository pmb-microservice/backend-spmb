<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pendaftar extends Model
{
    use HasFactory;
    
    protected $guarded = ['id'];
    protected $fillable = ['nama', 'email', 'tgllahir', 'prodi1', 'prodi2'];

}
