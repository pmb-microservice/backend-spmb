<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Http\Resources\PendaftarResource;
use App\Models\Pendaftar;
use Illuminate\Http\Request;

class PendaftarController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(),[
            'nama' => 'required|max:255',
            'email' => 'required|email',
            'tgllahir' => 'required|date',
            'prodi1' => 'required|integer',
            'prodi2' => 'integer',
        ]);
        

        if($validator->fails()){
            $msg = $validator->errors()->first();
            $res = [
                'status' => 400,
                'message' => $msg
            ];
            return response()->json($res, 400);       
        }

        $res = [
            'status' => 201,
            'message' => 'Berhasil mendaftar'
        ];
    return response()->json($res, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pendaftar  $pendaftar
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pendaftar = Pendaftar::find($id);
        if (is_null($pendaftar)) {
            $res = [
                'status' => 404,
                'message' => 'Pendaftar tidak ditemukan'
            ];
    
            return response()->json($res, 404);
        }
        $res = [
            'status' => 200,
            'data' => new PendaftarResource($pendaftar)
        ];
        return response()->json($res, 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pendaftar  $pendaftar
     * @return \Illuminate\Http\Response
     */
    public function edit(Pendaftar $pendaftar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pendaftar  $pendaftar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pendaftar $pendaftar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pendaftar  $pendaftar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pendaftar $pendaftar)
    {
        //
    }

}
